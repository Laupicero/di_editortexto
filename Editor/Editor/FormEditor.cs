﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Editor
{
    public partial class FormEditor : Form
    {
        private Font fuentePredterminada;
        public FormEditor()
        {
            InitializeComponent();
            copiarTsm.Enabled = false;
            pegarTsm.Enabled = false;
            this.fuentePredterminada = principalTextBox.Font;
        }


        //-------------------------------------------------
        //  Métodos: MenuStrip
        //-------------------------------------------------
        private void salirTsm_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }

        private void deshacerTsm_Click(object sender, EventArgs e)
        {
            if (principalTextBox.CanUndo)
            {
                principalTextBox.Undo();
            }            
        }

        private void cortarTsm_Click(object sender, EventArgs e)
        {
            Clipboard.SetData(DataFormats.Text, principalTextBox.SelectedText);
            principalTextBox.SelectedText = "";

            copiarTsm.Enabled = true;
            pegarTsm.Enabled = true;
        }

        private void copiarTsm_Click(object sender, EventArgs e)
        {
            Clipboard.SetData(DataFormats.Text, principalTextBox.SelectedText);
        }

        private void pegarTsm_Click(object sender, EventArgs e)
        {
            //principalTextBox.Text += Clipboard.GetText();
            principalTextBox.Paste();
        }


        //-------------------------------------------------
        //  Métodos:ToolStrip
        //-------------------------------------------------
        private void cortarTsb_Click(object sender, EventArgs e)
        {
            if(principalTextBox.SelectedText != "")
            {
                Clipboard.SetData(DataFormats.Text, principalTextBox.SelectedText);
                principalTextBox.SelectedText = "";

                copiarTsm.Enabled = false;
                pegarTsm.Enabled = false;
            }
            else
            {
                MessageBox.Show("Seleccione primero el texto", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
        }

        private void copiarTsb_Click(object sender, EventArgs e)
        {
            if(Clipboard.GetText() != "")
            {
                Clipboard.SetData(DataFormats.Text, principalTextBox.SelectedText);
            }
            else
            {
                MessageBox.Show("Copie primero algún texto", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }            
        }

        private void PegarTsb_Click(object sender, EventArgs e)
        {
            principalTextBox.Paste();
        }

        //-------------------------------------------------
        //  Estilo Letra
        //-------------------------------------------------
        private void courierNewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            courierNewToolStripMenuItem.Checked = true;
            arialToolStripMenuItem.Checked = false;
            predeterminadaToolStripMenuItem.Checked = false;

            principalTextBox.Font = new Font("Arial", principalTextBox.Font.Size);
        }

        private void arialToolStripMenuItem_Click(object sender, EventArgs e)
        {
            arialToolStripMenuItem.Checked = true;
            courierNewToolStripMenuItem.Checked = false;
            predeterminadaToolStripMenuItem.Checked = false;

            principalTextBox.Font = new Font("Courier New", principalTextBox.Font.Size);
        }

        private void predeterminadaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            predeterminadaToolStripMenuItem.Checked = true;
            courierNewToolStripMenuItem.Checked = false;
            arialToolStripMenuItem.Checked = false;

            principalTextBox.Font = this.fuentePredterminada;
        }


        //-------------------------------------------------
        //  Tamaño Fuente
        //-------------------------------------------------
        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            toolStripMenuItem2.Checked = true;
            toolStripMenuItem3.Checked = false;
            
            principalTextBox.Font = new Font(principalTextBox.Font.FontFamily, 16);
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            toolStripMenuItem3.Checked = true;
            toolStripMenuItem2.Checked = false;

            principalTextBox.Font = new Font(principalTextBox.Font.FontFamily, 24);
        }
    }
}
