﻿
namespace Editor
{
    partial class FormEditor
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirTsm = new System.Windows.Forms.ToolStripMenuItem();
            this.ediciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deshacerTsm = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.cortarTsm = new System.Windows.Forms.ToolStripMenuItem();
            this.copiarTsm = new System.Windows.Forms.ToolStripMenuItem();
            this.pegarTsm = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.cortarTsb = new System.Windows.Forms.ToolStripButton();
            this.copiarTsb = new System.Windows.Forms.ToolStripButton();
            this.PegarTsb = new System.Windows.Forms.ToolStripButton();
            this.principalTextBox = new System.Windows.Forms.TextBox();
            this.opcionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fuenteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.courierNewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.arialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.predeterminadaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tamañoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem,
            this.ediciónToolStripMenuItem,
            this.opcionesToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(774, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salirTsm});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.archivoToolStripMenuItem.Text = "Archivo";
            // 
            // salirTsm
            // 
            this.salirTsm.Image = global::Editor.Properties.Resources.exit;
            this.salirTsm.Name = "salirTsm";
            this.salirTsm.Size = new System.Drawing.Size(96, 22);
            this.salirTsm.Text = "Salir";
            this.salirTsm.Click += new System.EventHandler(this.salirTsm_Click);
            // 
            // ediciónToolStripMenuItem
            // 
            this.ediciónToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deshacerTsm,
            this.toolStripSeparator1,
            this.cortarTsm,
            this.copiarTsm,
            this.pegarTsm});
            this.ediciónToolStripMenuItem.Name = "ediciónToolStripMenuItem";
            this.ediciónToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.ediciónToolStripMenuItem.Text = "Edición";
            // 
            // deshacerTsm
            // 
            this.deshacerTsm.Image = global::Editor.Properties.Resources._return;
            this.deshacerTsm.Name = "deshacerTsm";
            this.deshacerTsm.Size = new System.Drawing.Size(147, 22);
            this.deshacerTsm.Text = "Deshacer";
            this.deshacerTsm.Click += new System.EventHandler(this.deshacerTsm_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(144, 6);
            // 
            // cortarTsm
            // 
            this.cortarTsm.Image = global::Editor.Properties.Resources.cut;
            this.cortarTsm.Name = "cortarTsm";
            this.cortarTsm.Size = new System.Drawing.Size(147, 22);
            this.cortarTsm.Text = "Cortar Ctrl+X";
            this.cortarTsm.Click += new System.EventHandler(this.cortarTsm_Click);
            // 
            // copiarTsm
            // 
            this.copiarTsm.Image = global::Editor.Properties.Resources.copy;
            this.copiarTsm.Name = "copiarTsm";
            this.copiarTsm.Size = new System.Drawing.Size(147, 22);
            this.copiarTsm.Text = "Copiar Ctrl+C";
            this.copiarTsm.Click += new System.EventHandler(this.copiarTsm_Click);
            // 
            // pegarTsm
            // 
            this.pegarTsm.Image = global::Editor.Properties.Resources.paste;
            this.pegarTsm.Name = "pegarTsm";
            this.pegarTsm.Size = new System.Drawing.Size(147, 22);
            this.pegarTsm.Text = "Pegar Ctrl+V";
            this.pegarTsm.Click += new System.EventHandler(this.pegarTsm_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cortarTsb,
            this.copiarTsb,
            this.PegarTsb});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(774, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // cortarTsb
            // 
            this.cortarTsb.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cortarTsb.Image = global::Editor.Properties.Resources.cut;
            this.cortarTsb.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cortarTsb.Name = "cortarTsb";
            this.cortarTsb.Size = new System.Drawing.Size(23, 22);
            this.cortarTsb.Text = "toolStripButton1";
            this.cortarTsb.Click += new System.EventHandler(this.cortarTsb_Click);
            // 
            // copiarTsb
            // 
            this.copiarTsb.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.copiarTsb.Image = global::Editor.Properties.Resources.copy;
            this.copiarTsb.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.copiarTsb.Name = "copiarTsb";
            this.copiarTsb.Size = new System.Drawing.Size(23, 22);
            this.copiarTsb.Text = "toolStripButton2";
            this.copiarTsb.Click += new System.EventHandler(this.copiarTsb_Click);
            // 
            // PegarTsb
            // 
            this.PegarTsb.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.PegarTsb.Image = global::Editor.Properties.Resources.paste;
            this.PegarTsb.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.PegarTsb.Name = "PegarTsb";
            this.PegarTsb.Size = new System.Drawing.Size(23, 22);
            this.PegarTsb.Text = "toolStripButton3";
            this.PegarTsb.Click += new System.EventHandler(this.PegarTsb_Click);
            // 
            // principalTextBox
            // 
            this.principalTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.principalTextBox.Font = new System.Drawing.Font("Microsoft YaHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.principalTextBox.Location = new System.Drawing.Point(0, 49);
            this.principalTextBox.MaximumSize = new System.Drawing.Size(790, 485);
            this.principalTextBox.Multiline = true;
            this.principalTextBox.Name = "principalTextBox";
            this.principalTextBox.Size = new System.Drawing.Size(774, 377);
            this.principalTextBox.TabIndex = 2;
            this.principalTextBox.Text = "Texto de prueba.";
            // 
            // opcionesToolStripMenuItem
            // 
            this.opcionesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fuenteToolStripMenuItem,
            this.toolStripSeparator2,
            this.tamañoToolStripMenuItem});
            this.opcionesToolStripMenuItem.Name = "opcionesToolStripMenuItem";
            this.opcionesToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.opcionesToolStripMenuItem.Text = "Opciones";
            // 
            // fuenteToolStripMenuItem
            // 
            this.fuenteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.courierNewToolStripMenuItem,
            this.arialToolStripMenuItem,
            this.predeterminadaToolStripMenuItem});
            this.fuenteToolStripMenuItem.Name = "fuenteToolStripMenuItem";
            this.fuenteToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.fuenteToolStripMenuItem.Text = "Fuente";
            // 
            // courierNewToolStripMenuItem
            // 
            this.courierNewToolStripMenuItem.Name = "courierNewToolStripMenuItem";
            this.courierNewToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.courierNewToolStripMenuItem.Text = "Courier New";
            this.courierNewToolStripMenuItem.Click += new System.EventHandler(this.courierNewToolStripMenuItem_Click);
            // 
            // arialToolStripMenuItem
            // 
            this.arialToolStripMenuItem.Name = "arialToolStripMenuItem";
            this.arialToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.arialToolStripMenuItem.Text = "Arial";
            this.arialToolStripMenuItem.Click += new System.EventHandler(this.arialToolStripMenuItem_Click);
            // 
            // predeterminadaToolStripMenuItem
            // 
            this.predeterminadaToolStripMenuItem.Name = "predeterminadaToolStripMenuItem";
            this.predeterminadaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.predeterminadaToolStripMenuItem.Text = "Predeterminada";
            this.predeterminadaToolStripMenuItem.Click += new System.EventHandler(this.predeterminadaToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(177, 6);
            // 
            // tamañoToolStripMenuItem
            // 
            this.tamañoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripMenuItem3});
            this.tamañoToolStripMenuItem.Name = "tamañoToolStripMenuItem";
            this.tamañoToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.tamañoToolStripMenuItem.Text = "Tamaño";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(180, 22);
            this.toolStripMenuItem2.Text = "16";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(180, 22);
            this.toolStripMenuItem3.Text = "24";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // FormEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(774, 426);
            this.Controls.Add(this.principalTextBox);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(790, 590);
            this.MinimumSize = new System.Drawing.Size(790, 0);
            this.Name = "FormEditor";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Editor";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirTsm;
        private System.Windows.Forms.ToolStripMenuItem ediciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deshacerTsm;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem cortarTsm;
        private System.Windows.Forms.ToolStripMenuItem copiarTsm;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.TextBox principalTextBox;
        private System.Windows.Forms.ToolStripMenuItem pegarTsm;
        private System.Windows.Forms.ToolStripButton cortarTsb;
        private System.Windows.Forms.ToolStripButton copiarTsb;
        private System.Windows.Forms.ToolStripButton PegarTsb;
        private System.Windows.Forms.ToolStripMenuItem opcionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fuenteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem courierNewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem arialToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem predeterminadaToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem tamañoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
    }
}

